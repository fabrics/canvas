# git関連
## ある言葉が含まれているコミットを探す
    git log -p -S"hogehoge"

## パッチファイルの吐き方、当て方
    git diff --binary > hoge.patch
    git apply hoge.patch
    git apply -R hoge.patch
* `git diff`は`git show`でも問題ない

## 他のブランチのファイルを取ってくる
    git checkout <branch> <file>

## ブランチを途中で切って他に移植する
    git rebase --onto <どこへ> <どこから> <どこまで>

## なんかCSVがみやすくなる
    git diff --word-diff-regex="[^,\n]+[,\n]|[,]" --color-words


