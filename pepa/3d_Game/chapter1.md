# 3Dゲームを面白くする技術

## 1章:3Dゲームをおもしろくするプレイヤーキャラの技術

### 1-1.2Dゲームプレイヤーを3Dゲームに引き込む技術(そもそも面白い2Dゲームの話)
####1.官能性

**運動や動作から得られる感覚的な気持ちよさ→「官能性」**

    例：
    Bダッシュで疾走するマリオを操作していて「気持ちいい」
    子供のころ意味もなく走りまわるのが楽しかった…(大人になって忘れてしまった気持ち)

####2．リスク&リターン

**リスクが大きいほど「緊張感」が大きくなり、リターンである「達成感」も大きく感じられる**

    例：
    確実に届くであろうジャンプ、もしかしたら届かないかもしれないと思いながらジャンプ 
    Bダッシュしながらの操作：早く移動しているため状況の判断を瞬時に行い瞬時に入力しなければならない
      →それが成功すると気持ちいい

####3．インタラクティブな遊び

**自分で何かを考えて、試してみて、そのフィードバックを得ることができる**

良くできたゲームは「アクション(試せること)」に対して多くの「リアクション(フィードバック)」が用意されている

    例：スーパーマリオ
    操作 →移動、Aボタン、Bボタン
    アクション →走る、ジャンプ、Bダッシュ、Bダッシュジャンプ、しゃがむ…
    リアクション →ブロックを押す、ブロックを壊す、ブロックに乗る、ブロックを飛び越える、土管に入る、アイテムを出す、アイテムを取る、敵を飛び越える、敵をつぶす…etc

**自由度の高いインタラクティブなゲーム**
>操作の複雑性(入力の組み合わせ)　<　アクションの数(試せることの数)　<　リアクションの数（フィードバックの数)

※リズムゲームなどは操作もアクションもリアクションも単純になるため上記の式は成立せず、インタラクティブな自由度は低くなる。その分、ゲームにおける「最適な答え(目的)」が単純明快になり、純粋に反射神経やリズム感が楽しめる。

####4．試したくなるフックを用意する

どんなにアクションやリアクションを豊富に用意しても、それを「試してみたい」と思わせられなければプレイヤーは行動してくれない。
**プレイヤーに思わず試してみたい！と感じさせる「ゲームのフック」が必要**

    例：
    ちびマリオがブロックをジャンプで頭突きするとブロックが動く→ブロックには何かあるぞという「心理的なフック」ができる
    ？ブロックを叩くとアイテムが出るということを知り、デカマリオだとブロックを壊せることが分かる
      →思わず色々なアクションを試してみたくなる！

###まとめ
1. プレイヤーを気持ちよくする**「官能性」**
2. ゲームのおもしろさを左右する**「チャレンジにおけるリスクとリターン」**
3. もっとジャンプしたくなる**「アクションとリアクション」**
4. 連続的・連鎖的に用意された**「フック」**

これらは「人を楽しませる体験」を提供するものであれば、ゲームに限らず**「全てのエンターテイメント」**に共通する基本的な要素である！！

#####感想
今までゲームをやりながら何となーく感じていた「面白い」をちゃんと分析し言葉にしてあって凄いなと思いました(小並感)
