# Unity5
## PersonalとProfessional違い
* 一番大きな違いは起動時のSplash画像を変更できるかできないかって部分みたい
* その他の機能的にはさほど大きな違いは無さそう？
### Professional版が使える外部機能
1. チームライセンス : 無料
* キャッシュサーバとバージョン管理ツールを含む Team License
* 基本的にはバージョン管理っぽい？大きなProjectを扱う時に色々とうまいことやってくれるらしい
2. Cloud Build Pro : 1年間 無料
* 様々なプラットフォームへデプロイ＆ビルドする際の時間と負担を大幅に軽減してくれる
3. Analytics Pro 無料
* Unityで制作し公開したゲームを、どのようにプレイヤーが遊んでいるかの統計分析を簡単にとることができる
4. Asset Store Level 11 : 無料
* AssetStoreで特別割引や優待を受けられる
※Personalでも金を払えばサービスを受けれるみたい

## FrameDebugger
* 1frameの内で、どういう順番で、何を描画してるかとかを順番に見れる
* Game実行中に瞬時にデバッグすることができる
* どこでどういう処理をやっているかが一目でわかるため無駄な処理をすぐに見つけることができる
* 描画を順番に見れてテンションが上がる
### FrameDebuggerの使い方
1. ツールバーの Window → FrameDebugger にある(Personal版でも使えるみたい)
* 気になる瞬間にEnableボタンを押せばそのフレームの情報が見れる
2. あとは気になる場所を1つずつ確認するだけ(情報を見るのに描画に関する知識が求められるかも…)
* <http://qiita.com/yasei_no_otoko/items/1573a8a4944f5e5142fa>

## Mute機能の実装
* 地味にうれしいMute機能！
* GameViewの上の方にあるよ！
* これでゲーム内のBGMやSEを無視して好きな音楽を聴くことができる！やったね！！

## SpritePacker
* SpritePackerが使えるようになったらしい
* これで外部のAtlas作成ツールに頼らなくてもよくなるかも？

## Android/iOSでSocketが使用可能に
* Unity4ではAndroid/iOSのProを買わなければSocketが使用できなかった
* 今まではUnityのNetwork機能のRPCがあったが使いにくかった
* これからは無料で普通にSocketを使ってアプリ作れるかも
