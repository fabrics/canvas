#UnityVS使おう
----
####UnityVSのインストール

1. MicrosoftからUnityVSの自分が持ってるVSのversionに合ったインストーラーを落としてくる
    * <https://msdn.microsoft.com/ja-jp/dn833194.aspx>
    * **※ vs community 2013 なら完全に無料で使える!**
2. UnityにUnityVSpackageをImport
    * `Assets -> ImportPackage -> CustumPackage`を選択
    * `C:\Program Files (x86)\Microsoft Visual Studio Tools for Unity`
    * ↑この中のVSのversionにあったフォルダの中に.unitypackageがあるので選択しunityにdllをImport　
3. かんりょう！
    * UnityのToolBarに`Visual Studio Tools`ってのが出てるはず

####UnityVS用のプロジェクトを作る

1. UnityVS用のprojectを作成
    * `Visual Studio Tools` -> `Genarate Project files`
2. UnityVSでVisualStudioを開く
    * `Visual Studio Tools` -> `Open in Visual Studio`

↑ここまで導入

----

↓ここから実際の使用方法
####UnityVSを使う
######BreakPointを貼ってデバッグ
 1. 普通にVisualStudioでBreakPointを貼る
 2. VS側でToolBarにある`Attach to Unity`ボタンを押す
 3. Unity側で実行
 4. BreakPointで止まる！！（はず）
######MonoBehaviourクラスが持ってるメソッドを生成
 1. VS上で右クリック -> `Implement MonoBehaviours` or `Ctrl + Shift + M`
 2. または右クリック -> `Quick MonoBehaviours` or `Ctrl + Shift + Q`
 3. 一覧表示されるので使うメソッドを選択してOK
######VS上からUnityのReferenceを見る
- `ヘルプ` -> `Unity API Reference`
######VS上からUnityのファイル階層を見る
- `表示` -> `Unity Project Explorer` or `Shift + Alt + E`
